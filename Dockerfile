FROM maven:3-jdk-8 AS build
WORKDIR /usr/src
COPY . .
RUN mvn clean install

FROM openjdk:8-alpine
COPY --from=build /usr/src/target/shop-1.0.jar /usr/src/shop-1.0.jar
EXPOSE 8990
ENTRYPOINT java -jar /usr/src/shop-1.0.jar
